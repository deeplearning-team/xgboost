#!/bin/bash
# run this script in the source root, i.e., outside of the debian/ directory.
set -e
set -x
if test -d gputreeshap; then rm -rfv gputreeshap; fi
wget -c https://github.com/rapidsai/gputreeshap/archive/787259b412c18ab8d5f24bf2b8bd6a59ff8208f3.tar.gz -O gputreeshap.tar.gz
mkdir -p gputreeshap
tar xvf gputreeshap.tar.gz -C gputreeshap --strip-components=1

rm -rfv gputreeshap/build.sh
rm -rfv gputreeshap/Doxyfile.in
#rm -rfv gputreeshap/GPUTreeShap
#rm -rfv gputreeshap/GPUTreeShap/gpu_treeshap.h
#rm -rfv gputreeshap/LICENSE
rm -rfv gputreeshap/example
rm -rfv gputreeshap/example/example.cu
rm -rfv gputreeshap/ci
rm -rfv gputreeshap/ci/checks
rm -rfv gputreeshap/ci/checks/style.sh
rm -rfv gputreeshap/ci/gpu
rm -rfv gputreeshap/ci/gpu/build.sh
#rm -rfv gputreeshap/CMakeLists.txt
rm -rfv gputreeshap/benchmark
rm -rfv gputreeshap/benchmark/benchmark.py
rm -rfv gputreeshap/benchmark/format_results.py
rm -rfv gputreeshap/benchmark/benchmark_gpu_treeshap.cu
rm -rfv gputreeshap/cmake
rm -rfv gputreeshap/cmake/doxygen.cmake
rm -rfv gputreeshap/.gitignore
rm -rfv gputreeshap/tests
rm -rfv gputreeshap/tests/test_utils.h
rm -rfv gputreeshap/tests/test_gpu_treeshap.cu
#rm -rfv gputreeshap/README.md

mv gputreeshap debian/
